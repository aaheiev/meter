namespace :load do

  desc "Loading meters info"
  task :meters => :environment do
    meters_data = YAML::load_file(Jets.root.join('db', 'data','meters.yaml'))
    meters_data.each do |m|
      puts m['name']
      unless meter = Meter.find(m['name'])
        meter = Meter.new(m)
      else
        meter.attrs(m)
      end
      meter.replace
      # puts m.to_json
    end
    # [].each do |item|
    #   puts item[:name]
    #   unless project = Project.find(item[:name])
    #     project = Project.new(name: item[:name], next_build_id: Jets.config.initial_id)
    #   end
    #   project.attrs( { team: item[:dev_team_name], repo_url: item[:scm_repo_url] } )
    #   project.replace
    # end
  end

end