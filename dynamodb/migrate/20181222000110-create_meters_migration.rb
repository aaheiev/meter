class CreateMetersMigration < Dynomite::Migration
  def up
    create_table :meters do |t|
      t.partition_key "name:string:hash" # required
    end

    update_table :meters do |t|

      t.gsi(:create, "location-index") do |i|
        i.partition_key "location:string"
      end

    end

    update_table :meters do |t|

      t.gsi(:create, "type-index") do |i|
        i.partition_key "type:string"
      end

    end

  end
end

# More examples: https://github.com/tongueroo/dynomite/tree/master/docs
